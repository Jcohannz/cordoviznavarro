package codewars;

public class ProdFib {

    public static long[] productFib(long prod) {
        long lFib = 0; long hFib = 1;
        long[] ans = new long[3];
        while(lFib*hFib <= prod) {
            if(lFib * hFib == prod) {
                ans[0] = lFib; ans[1] = hFib; ans[2] = 1;
                return ans;
            }
            hFib += lFib;
            lFib = hFib - lFib;
        }
        ans[0] = lFib; ans[1] = hFib; ans[2] = 0;
        return ans;
    }
}