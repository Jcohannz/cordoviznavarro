package codewars;

public class SumOfPositive {
    public static int sum(int[] arr) {
        int sum = 0;
        int i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                sum += arr[i];
            }
        }
        return sum;
    }
}
