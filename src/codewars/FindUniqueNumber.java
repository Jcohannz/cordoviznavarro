package codewars;

import java.util.Arrays;

public class FindUniqueNumber {
    public static double findUniq(double arr[]) {
        // Do the magic
        Arrays.sort( arr );
        if (arr[0] == arr[1])
            return arr[arr.length - 1];
        else
            return arr[0];
    }
}
