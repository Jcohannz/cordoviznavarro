package codewars;

public class FindOdd {
    public static int findIt(int[] array) {
        for(int i = 0; i < array.length; i++){
            int num = array[i];
            int occurences = 0;

            for (int j = 0; j < array.length; j++){
                if (array[j] == num){
                    occurences++;
                }
            }

            if (occurences % 2 != 0){
                return num;
            }

        }

        return 0;
    }

}